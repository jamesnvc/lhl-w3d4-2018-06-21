//
//  NetworkManager.m
//  NetworkingDemo
//
//  Created by James Cash on 21-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "NetworkManager.h"

@implementation NetworkManager

+ (void)fetchRepresentativesForPostalCode:(NSString*)postalCode completion:(void (^)(NSArray<Representative*>*))completion;
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://represent.opennorth.ca/postcodes/%@/?sets=federal-electoral-districts", postalCode]];

    // in this case, because we're making an HTTP GET request to the url and we don't need to set any other headers, body, or change the method (from the default of GET), we can use the dataTaskWithURL: method
    // if we did need to do any of those things, we would create a NSURLRequest from the NSURL above & use dataTaskWithRequest: instead
    NSLog(@"Creating task");
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData* data, NSURLResponse*  response, NSError * error) {
        NSLog(@"Got response: %@", response);

        if (error) {
            NSLog(@"Error running request: %@", error.localizedDescription);
            completion(nil);
            return;
        }

        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        if (httpResponse.statusCode != 200) {
            NSLog(@"Bad response from API: %@", httpResponse);
            completion(nil);
            return;
        }

//        NSLog(@"Data: %@", data);
        NSError *err = nil;
        NSDictionary *resultData = [NSJSONSerialization JSONObjectWithData:data
                                        options:0
                                          error:&err];
        if (err != nil) {
            NSLog(@"Error parsing json: %@", err.localizedDescription);
            completion(nil);
            return;
        }

        NSArray *repsJson = resultData[@"representatives_centroid"];
        NSMutableArray *repsData = [@[] mutableCopy];
        for (NSDictionary *rep in repsJson) {
            NSLog(@"Representative: %@", rep);
            Representative *r = [[Representative alloc] initWithAPIData:rep];
            [repsData addObject:r];
        }
        NSArray<Representative*>* result = [repsData copy];
        completion(result);

    }];
    NSLog(@"Created task");
    [task resume];
    NSLog(@"Started task");
}

@end
