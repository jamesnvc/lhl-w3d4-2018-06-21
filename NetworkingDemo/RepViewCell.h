//
//  RepViewCell.h
//  NetworkingDemo
//
//  Created by James Cash on 21-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
