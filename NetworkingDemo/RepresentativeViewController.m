//
//  RepresentativeViewController.m
//  NetworkingDemo
//
//  Created by James Cash on 21-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "RepresentativeViewController.h"

@interface RepresentativeViewController ()
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UILabel *partyNameLabel;

@end

@implementation RepresentativeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.nameLabel.text = self.rep.name;
    self.emailLabel.text = self.rep.email;
    self.partyNameLabel.text = self.rep.party;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
