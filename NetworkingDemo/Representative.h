//
//  Representative.h
//  NetworkingDemo
//
//  Created by James Cash on 21-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Representative : NSObject

@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *party;

- (instancetype)initWithAPIData:(NSDictionary*)data;

@end
