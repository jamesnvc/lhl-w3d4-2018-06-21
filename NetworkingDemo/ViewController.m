//
//  ViewController.m
//  NetworkingDemo
//
//  Created by James Cash on 21-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "NetworkManager.h"
#import "RepViewCell.h"
#import "RepresentativeViewController.h"

@interface ViewController () <UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) NSArray *reps;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [NetworkManager fetchRepresentativesForPostalCode:@"M5V2H8" completion:^(NSArray<Representative*> *reps) {
        self.reps = reps;
        // because this is on a background thread, when we go to update the tableview, we need to do that part on the main thread
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.collectionView reloadData];
        }];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showRepDetail"]) {
        RepresentativeViewController* dvc = segue.destinationViewController;
        NSIndexPath *indexPath = [[self.collectionView indexPathsForSelectedItems] firstObject];
        Representative* selectedRep = self.reps[indexPath.item];
        dvc.rep = selectedRep;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.reps.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RepViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"repCell" forIndexPath:indexPath];

    Representative* rep = self.reps[indexPath.item];
    cell.nameLabel.text = rep.name;

    return cell;
}

@end
