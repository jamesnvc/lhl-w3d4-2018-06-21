//
//  NetworkManager.h
//  NetworkingDemo
//
//  Created by James Cash on 21-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Representative.h"

@interface NetworkManager : NSObject

+ (void)fetchRepresentativesForPostalCode:(NSString*)postalCode completion:(void (^)(NSArray<Representative*>*))completion;

@end
