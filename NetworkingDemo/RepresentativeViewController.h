//
//  RepresentativeViewController.h
//  NetworkingDemo
//
//  Created by James Cash on 21-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Representative.h"

@interface RepresentativeViewController : UIViewController

@property (nonatomic,strong) Representative* rep;

@end
