//
//  Representative.m
//  NetworkingDemo
//
//  Created by James Cash on 21-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "Representative.h"

@implementation Representative

- (instancetype)initWithAPIData:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        _name = dict[@"name"];
        _email = dict[@"email"];
        _party = dict[@"party_name"];
    }
    return self;
}
@end
